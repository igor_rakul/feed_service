﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FeedService.Abstract
{
    public interface IFeedFormatter
    {
        bool CanRead(XmlReader reader);
        void ReadFrom(XmlReader reader);
        string ToString();

    }
}
