﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedService.Models
{
    public class UserFeedChannel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Category { get; set; }
        public int FeedChannelId { get; set; }
    }
}