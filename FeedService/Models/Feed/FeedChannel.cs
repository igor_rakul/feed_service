﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedService.Models
{
    public class FeedChannel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }

    }
}