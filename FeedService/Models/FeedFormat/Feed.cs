﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FeedService.Abstract;

namespace FeedService.Models

{
    public class Feed
    {
        public Feed()
        {
            FeedItems = new List<FeedItem>();
        }

        public List<FeedItem> FeedItems { get; set; }
    }
}