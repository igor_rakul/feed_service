﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedService.Models
{ 


    public class FeedItem
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        
    }
}