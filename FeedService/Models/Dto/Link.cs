﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedService.Models
{
    public class Link
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string FeedLink { get; set; }
    }
}