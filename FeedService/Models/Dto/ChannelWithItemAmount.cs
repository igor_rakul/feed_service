﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FeedService.Models
{
    public class ChannelWithItemAmount
    {
        public int ChannelId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public int NumberOfItems { get; set; }
    }
}