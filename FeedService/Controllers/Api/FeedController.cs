﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using FeedService.Models;
using System.ServiceModel.Syndication;
using FeedService.Abstract;
using System.Text;
using System.IO;
using System.Xml;
using Microsoft.AspNet.Identity;

namespace FeedService.Controllers.Api
{
    
    public class FeedController : ApiController
    {
        private ApplicationDbContext _context;
        

        public FeedController()
        {
            _context = new ApplicationDbContext();
        }


        // GET: api/Feed
        [HttpGet]
        public IHttpActionResult Get(string query = null)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return BadRequest();
            }

            // get id of current user
            var currentUserId = User.Identity.GetUserId();
            
            // select feed ids from db
            var userChannelsIds = _context.UserFeedChannels.Where(u => u.UserId == currentUserId).Select(c => c.FeedChannelId).ToList();

            // select feed from db
            var channelsQuery = _context.FeedChannels.Where(c => userChannelsIds.Contains(c.Id)).ToList();
            var numberOfItems = new List<int>();


            var feedResult = new List<ChannelWithItemAmount>();

            foreach (var c in channelsQuery)
            {
                XmlReader reader = XmlReader.Create(c.Link);

                // Read an Xml to SyndicationFeed format 
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();

                var res = new ChannelWithItemAmount
                {
                    ChannelId = c.Id,
                    Name = c.Name,
                    Link = c.Link,
                    NumberOfItems = feed.Items.Count()
                };
                feedResult.Add(res);
            }



            return Ok(feedResult);
        }

        // GET: api/Feed/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
        
            // Get a link for a feed channel from Db
            var channelLink = _context.FeedChannels.SingleOrDefault(f => f.Id == id).Link;

            XmlReader reader = XmlReader.Create(channelLink);

            // Read an Xml to SyndicationFeed format 
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            reader.Close();

            // Create an instance of our own Feed format
            Feed _feed = new Feed();
            
            // Transform SyndicationItem into own format FeedItem
            foreach (SyndicationItem item in feed.Items)
            {
                FeedItem _feedItem = new FeedItem
                {
                    Title = item.Title.Text,
                    Description = item.Summary.Text,
                    Link = item.Links.FirstOrDefault().GetAbsoluteUri().AbsoluteUri
                };

                _feed.FeedItems.Add(_feedItem);
            }


            return Ok(_feed);
            
        }

        // POST: api/Feed
        [HttpPost]
        public IHttpActionResult AddUserFeedChannel (Link link)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return BadRequest();
            }
           
            // create a new feed channel
            FeedChannel f = new FeedChannel
            {
                Id = 0,
                Name = link.Name,
                Link = link.FeedLink
            };
            // add a new feed channel to Db
            _context.FeedChannels.Add(f);
            _context.SaveChanges();

            // select channel id which has just been added
            var channelId = _context.FeedChannels.FirstOrDefault(c => c.Name == f.Name).Id;

            // add user id and channel id to db
            var _userFeedChannel = new UserFeedChannel
            {
                Id = 0,
                UserId = User.Identity.GetUserId(),
                FeedChannelId = channelId,
                Category = link.Category
            };
            _context.UserFeedChannels.Add(_userFeedChannel);
            _context.SaveChanges();

            return Ok();

    
        }

        // PUT: api/Feed/5
        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Feed/5
        [HttpDelete]
        public void Delete(int id)
        {
        }

        
    }
}
