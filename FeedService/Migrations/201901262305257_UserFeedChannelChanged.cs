namespace FeedService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserFeedChannelChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserFeedChannels", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserFeedChannels", "UserId", c => c.Int(nullable: false));
        }
    }
}
