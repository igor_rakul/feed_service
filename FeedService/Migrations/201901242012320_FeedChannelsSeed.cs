namespace FeedService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FeedChannelsSeed : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT [dbo].[FeedChannels] ON
                INSERT INTO [dbo].[FeedChannels] ([Id], [Name], [Link]) VALUES (1, N'CNN World News', N'http://rss.cnn.com/rss/edition_world.rss')
                INSERT INTO [dbo].[FeedChannels] ([Id], [Name], [Link]) VALUES (2, N'BBC World News', N'http://feeds.bbci.co.uk/news/world/rss.xml')
                INSERT INTO [dbo].[FeedChannels] ([Id], [Name], [Link]) VALUES (3, N'BBC Technology News', N'http://feeds.bbci.co.uk/news/technology/rss.xml')
            SET IDENTITY_INSERT [dbo].[FeedChannels] OFF

            ");
        }
        
        public override void Down()
        {
            Sql(@"
                DELETE [dbo].[FeedChannels]
                WHERE Id IN (1,2,3)
");

                
        }
    }
}
