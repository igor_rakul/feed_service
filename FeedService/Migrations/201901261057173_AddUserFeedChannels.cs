namespace FeedService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserFeedChannels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserFeedChannels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Category = c.String(),
                        FeedChannelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            

        }
        
        public override void Down()
        {

            DropTable("dbo.UserFeedChannels");
        }
    }
}
